-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 12, 2022 at 12:51 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 7.4.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `prime`
--

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE `history` (
  `hisid` int(11) NOT NULL,
  `startnum` int(11) NOT NULL,
  `endnum` int(11) NOT NULL,
  `primenum` text NOT NULL,
  `countnum` int(11) NOT NULL,
  `hisdate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `history`
--

INSERT INTO `history` (`hisid`, `startnum`, `endnum`, `primenum`, `countnum`, `hisdate`) VALUES
(4, 10, 500, '11 , 13 , 17 , 19 , 23 , 29 , 31 , 37 , 41 , 43 , 47 , 53 , 59 , 61 , 67 , 71 , 73 , 79 , 83 , 89 , 97 , 101 , 103 , 107 , 109 , 113 , 121 , 127 , 131 , 137 , 139 , 143 , 149 , 151 , 157 , 163 , 167 , 169 , 173 , 179 , 181 , 187 , 191 , 193 , 197 , 199 , 209 , 211 , 221 , 223 , 227 , 229 , 233 , 239 , 241 , 247 , 251 , 253 , 257 , 263 , 269 , 271 , 277 , 281 , 283 , 289 , 293 , 299 , 307 , 311 , 313 , 317 , 319 , 323 , 331 , 337 , 341 , 347 , 349 , 353 , 359 , 361 , 367 , 373 , 377 , 379 , 383 , 389 , 391 , 397 , 401 , 403 , 407 , 409 , 419 , 421 , 431 , 433 , 437 , 439 , 443 , 449 , 451 , 457 , 461 , 463 , 467 , 473 , 479 , 481 , 487 , 491 , 493 , 499 , ', 114, '2022-04-12 08:59:32'),
(5, 1, 50, '1 , 2 , 3 , 5 , 7 , 11 , 13 , 17 , 19 , 23 , 29 , 31 , 37 , 41 , 43 , 47 , ', 16, '2022-04-12 09:00:16'),
(7, 1, 20, '1 , 2 , 3 , 5 , 7 , 11 , 13 , 17 , 19 , ', 9, '2022-04-12 10:10:53'),
(8, 1, 30, '1 , 2 , 3 , 5 , 7 , 11 , 13 , 17 , 19 , 23 , 29 , ', 11, '2022-04-12 10:11:08'),
(9, 1, 10, '1 , 2 , 3 , 5 , 7 , ', 5, '2022-04-12 10:11:15'),
(10, 10, 100, '11 , 13 , 17 , 19 , 23 , 29 , 31 , 37 , 41 , 43 , 47 , 53 , 59 , 61 , 67 , 71 , 73 , 79 , 83 , 89 , 97 , ', 21, '2022-04-12 10:11:17'),
(11, 1, 50, '1 , 2 , 3 , 5 , 7 , 11 , 13 , 17 , 19 , 23 , 29 , 31 , 37 , 41 , 43 , 47 , ', 16, '2022-04-12 10:11:22'),
(12, 1, 10, '1 , 2 , 3 , 5 , 7 , ', 5, '2022-04-12 10:20:25'),
(13, 1, 20, '1 , 2 , 3 , 5 , 7 , 11 , 13 , 17 , 19 , ', 9, '2022-04-12 10:20:28'),
(14, 1, 50, '1 , 2 , 3 , 5 , 7 , 11 , 13 , 17 , 19 , 23 , 29 , 31 , 37 , 41 , 43 , 47 , ', 16, '2022-04-12 10:20:32'),
(15, 1, 20, '1 , 2 , 3 , 5 , 7 , 11 , 13 , 17 , 19 , ', 9, '2022-04-12 10:24:32');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`hisid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `history`
--
ALTER TABLE `history`
  MODIFY `hisid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
